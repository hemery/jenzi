import rclpy
from rclpy.node import Node

from yeux_interface.msg import Color
from yeux_interface.srv import SetColor
from yeux_interface.srv import Active
     
        

class YeuxPublisher(Node):

    def __init__(self):
        super().__init__('yeux_publisher')
        self.publisher_ = self.create_publisher(Color, 'color', 10)
        self.srv = self.create_service(Active, 'active_yeux', self.active_yeux)
        self.srv_color = self.create_service(Active, 'active_color', self.active_auto_color)
        self.srv_clignement = self.create_service(Active, 'active_clignement', self.active_clignement)
        self.srv_set_color = self.create_service(SetColor, 'set_color', self.set_color)
        timer_period = 12
        delta = 0.1
        timer_period_clignement_debut = 5
        timer_period_clignement_fin = timer_period_clignement_debut + delta
        
        self.timer_period_color = self.create_timer(timer_period, self.timer_callback_color)
        self.timer_period_clignement_debut = self.create_timer(timer_period_clignement_debut, self.timer_callback_clignement)
        self.timer_period_clignement_fin = self.create_timer(timer_period_clignement_fin, self.timer_callback_clignement)
        self.i = 0
        self.red = True
        self.blue = False
        self.green = False
        self.power = True

    def setup_color(self):
        color = Color()
        color.r = self.red;
        color.g = self.green;
        color.b = self.blue
        color.time = 20
        color.pow = self.power;
        color.angle = 100

        return color

    def info_color(self):
        if self.red:
            self.get_logger().info("Changement couleur rouge")
        elif self.blue:
            self.get_logger().info("Changement couleur bleu")
        elif self.green:
            self.get_logger().info("Changement couleur vert")

    def clignement(self):
        self.power = not self.power
        self.get_logger().info("Clignement")

    def change_color(self):
        if self.red:
            self.red = False
            self.green = False
            self.blue = True
        elif self.blue:
            self.red = False
            self.green = True
            self.blue = False
        elif self.green:
            self.red = True
            self.green = False
            self.blue = False
        self.info_color()

    def timer_callback_clignement(self):
        self.clignement()
        color = self.setup_color()
        self.publisher_.publish(color)
        
    def timer_callback_color(self):
        self.change_color()
        color = self.setup_color()
        self.publisher_.publish(color)


    # Changer la couleur
    # ros2 service call /set_color yeux_interface/src/SetColor "{color : "red"}"
    def set_color(self, request, response):
        self.timer_period_color.cancel()
        if request.color == "red":
            self.red = True
            self.green = False
            self.blue = False
        elif request.color == "blue":
            self.blue = True
            self.red = False
            self.green = False
        elif request.color == "green":
            self.blue = False
            self.red = False
            self.green = True
        self.info_color()
        color = self.setup_color()
        self.publisher_.publish(color)
        return response

    # Active ou désactive le clignement automatique
    # ros2 service call /active_clignement yeux_interface/srv/Active "{power : true}"
    def active_clignement(self, request, response):
        if request.power:
            self.get_logger().info("Active le clignement")
            self.timer_period_clignement_debut.reset()
            self.timer_period_clignement_fin.reset()
        else:
            self.get_logger().info("Désactive le clignement")
            self.timer_period_clignement_debut.cancel()
            self.timer_period_clignement_fin.cancel()
        return response

    # Active ou désactive le changement de couleur automatique
    # ros2 service call /active_color yeux_interface/srv/Active "{power : true}"
    def active_auto_color(self, request, response):
        if request.power:
            self.get_logger().info("Active le changement de couleur automatique")
            self.timer_period_color.reset()
        else:
            self.get_logger().info("Désactive le changement de couleur automatique")
            self.timer_period_color.cancel()
        return response

    # Active ou désactive l'oeuil
    # ros2 service call /active_yeux yeux_interface/srv/Active "{power: true}"
    def active_yeux(self, request, response):
        self.power = request.power
        if request.power:
            self.get_logger().info("Allumage de l'oeuil")
            self.timer_period_clignement_debut.reset()
            self.timer_period_clignement_fin.reset()
        else:
            self.get_logger().info("Extinction de l'oeuil")
            self.timer_period_clignement_debut.cancel()
            self.timer_period_clignement_fin.cancel()
            
        color = self.setup_color()
        self.publisher_.publish(color)
        return response
        

def main(args=None):
    rclpy.init(args=args)

    yeux_publisher = YeuxPublisher()

    rclpy.spin(yeux_publisher)

    yeux_publisher.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
