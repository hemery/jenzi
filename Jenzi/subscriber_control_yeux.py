import time
import serial
import rclpy
from rclpy.node import Node

from yeux_interface.msg import Color


class YeuxSubscriber(Node):
	
	def __init__(self):
		super().__init__('yeux_subscriber')
		self.ser = serial.Serial('/dev/ttyACM0', 57600, timeout=1.5)
		time.sleep(2)
		self.subscription = self.create_subscription(
			Color,
			'color',
			self.listener_callback,
			10)
		self.subscription
		
	def listener_callback(self, msg):
                commande_r = "set r " + str(int(msg.r)) + "\r\n"
                commande_g = "set g " + str(int(msg.g)) + "\r\n"
                commande_b = "set b " + str(int(msg.b)) + "\r\n"
                commande_pow = "set " + str(int(msg.pow)) + "\r\n"
                commande_time = "set t " + str(int(msg.time)) + "\r\n"
                commande_angle = "set a " + str(int(msg.angle)) + "\r\n"
                self.ser.write(commande_pow.encode("ascii"))
                self.ser.write(commande_r.encode("ascii"))
                self.ser.write(commande_g.encode("ascii"))
                self.ser.write(commande_b.encode("ascii"))
                self.ser.write(commande_time.encode("ascii"))
                self.ser.write(commande_angle.encode("ascii"))
                self.get_logger().info('active rouge : "%s"' % msg.r)
                self.get_logger().info('active vert : "%s"' % msg.g)
                self.get_logger().info('active bleu : "%s"' % msg.b)
                self.get_logger().info('active power : "%s"' % msg.pow)
                self.get_logger().info('active time : "%s"' % msg.time)
                self.get_logger().info('active angle : "%s"' % msg.angle)

def main(args=None):
    rclpy.init(args=args)
    yeux_subscriber = YeuxSubscriber()
    
    rclpy.spin(yeux_subscriber)
    
    ser.close()
    yeux_subscriber.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
